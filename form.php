<?php

require_once 'email.php';
require_once 'form_validation.php';

class ContactForm{

    public $nome;
    public $email;
    public $assunto;
    public $mensagem;

    function __construct() {
        $this->nome     = filter_var($_POST['nome'], FILTER_SANITIZE_SPECIAL_CHARS);
        $this->email    = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
        $this->assunto  = filter_var($_POST['assunto'], FILTER_SANITIZE_SPECIAL_CHARS);
        $this->mensagem = filter_var($_POST['mensagem'], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    public function sendPost($destinatario)
    {
        $errors = array();
        $validator = new FormValidation();
        
        if(!$validator->isNome($this->nome))
            $errors['nome'] = "preencha seu nome corretamente";
        
        if(!$validator->isEmail($this->email))
            $errors['email'] = "preencha seu email corretamente"; 
        
        if($validator->isEmpty($this->assunto))
            $errors['assunto'] = "preencha o campo 'assunto'";
         
         if($validator->isEmpty($this->mensagem))
            $errors['mensagem'] = "preencha o campo 'mensagem'";

         if(empty($errors)){
            $email = new Email($this->nome, $this->email, $this->assunto, $this->mensagem, $destinatario);
            $email->send();
            $success['sucesso'] =  "Email enviado com sucesso!";
            echo json_encode($success);
         } else {
            $errors['errors'] = "true";
            echo json_encode($errors);
          }
    }
}