<?php 

class Email {

    public $nome;
    public $email;
    public $assunto;
    public $mensagem;
    public $status;
    public $destinatario;

    function __construct($nome, $email, $assunto, $mensagem, $destinatario)
    {
        $this->nome         = $nome;
        $this->email        = $email;
        $this->assunto      = $assunto;
        $this->mensagem     = $mensagem;
        $this->destinatario = $destinatario;               
    }

    private function createHeaders()
    {
        $headers = array();
        
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=utf-8";
        $headers[] = "From: $this->email";
        $headers[] = "Reply-To: $this->email";
        $headers[] = "Assunto: $this->assunto";
        
        return implode("\r\n", $headers);

    }

    private function createBody()
    {
        $body = array();

        $body[] = "Contato - Site";
        $body[] = "Nome: ". $this->nome;
        $body[] = "Email: ". $this->email;
        $body[] = "Mensagem \r\n\n". $this->mensagem;

        return implode("\r\n", $body);
    }

    public function send()
    {
        $headers = $this->createHeaders();
        $body = $this->createBody();    
        mail($this->destinatario, $this->assunto, $body, $headers);
    }

}