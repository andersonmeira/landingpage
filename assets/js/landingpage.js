app = (function() {

    function init() {

        $("#form_contato :input").on('focus', function() {
            if ($(this).is(':text') || $(this).is('[type=email]') || $(this).is('textarea')) {
                $(this).next().empty();
            }
        });

        $('#enviar').on('click', sendEmail);
    }

    function sendEmail() {

        var form = $('#form_contato');
        dados = form.serialize();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'contato.php',
            async: true,
            data: dados,
            success: function(response) {
                if (response.errors === 'true') {
                    if (response.nome)
                        $('#error_nome').text(response.nome);
                    if (response.email)
                        $('#error_email').text(response.email);
                    if (response.assunto)
                        $('#error_assunto').text(response.assunto);
                    if (response.mensagem)
                        $('#error_mensagem').text(response.mensagem);
                } else {
                    $('#msg_sucesso')
                        .toggleClass('hidden')
                        .text(response.sucesso);
                    form.find(':text').val('');
                    form.find('textarea').val('');
                    form.find('[type=email]').val('');
                }
            }
        });
    }

    return {
        init: init,
        sendEmail: sendEmail
    };

})();

$(document).ready(function() {
    app.init();
});