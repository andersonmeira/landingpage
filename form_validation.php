<?php

class FormValidation {
    
    public function isNome($nome)
    {
        if(strlen($nome) >=3 && preg_match('/[A-Z]+[a-z]/', $nome))
            return true;
        else 
            return false;
    }

    public function isEmail($email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }

    public function isEmpty($arg)
    {
        if(empty($arg))
            return true;
        else
            return false;
    }
}