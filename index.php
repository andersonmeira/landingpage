<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Vertical 3W - Gestão de Projetos | Outsourcing | Fábrica de Software</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Gestão de Projetos, Outsourcing, Fábrica de Sotware" />
        <meta name="keywords" content="PMO, ,Gestão de Projetos, Desenvolvimento, Software, soluções, v3w, outsourcing" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-48873060-1', 'vertical3w.com.br');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row content">
                <div class="col-md-12">
                    <div class="col-md-5 col-md-offset-1 aviso text-left">
                        <img src="assets/img/logo.png" alt="Vertical 3W">
                        <h2>Olá!</h2>
                        <p>
                            Enquanto convertemos xícaras de café em um novo site cheio de novidades, 
                            você pode entrar em contato conosco através do formulário ao lado!
                        </p>
                        <div class="row">
                            <div class="col-md-6 address-box">
                                <address>
                                    <strong>© 2013 Vertical3W</strong><br>
                                    Av. Maria Fernandes Cavallari, 1655 - Sala 5<br>
                                    CEP: 17526-431<br>
                                    Marília - SP<br>
                                    Fone: (14) 3316-1112<br>      
                                </address>
                            </div>
                            <div class="col-md-6 text-right">
                                <img src="assets/img/logo-ciem.png" alt="Centro Incubador de Empresas Miguel Silva">
                            </div>
                        </div>    
                    </div>        
                    <div class="col-md-4 col-md-offset-1 form-content text-left">
                        <form id="form_contato" class="form-horizontal" role="form" action="" method="post">
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">Nome:</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nome" class="form-control" placeholder="Seu Nome">
                                    <span id="error_nome" class="label label-warning"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email:</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                    <span id="error_email" class="label label-warning"></span>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="assunto" class="col-sm-3 control-label">Assunto:</label>
                                <div class="col-sm-9">
                                    <input type="text" name="assunto" class="form-control" placeholder="Assunto">
                                    <span id="error_assunto" class="label label-warning"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Mensagem" class="col-sm-3 control-label">Mensagem:</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="mensagem" id="" cols="30" rows="10"></textarea>
                                    <span id="error_mensagem" class="label label-warning"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <input id="enviar" type="button" class="btn btn-primary btn-lg" value="enviar" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="msg_sucesso" class="alert alert-success hidden col-md-offset-4 col-sm-8"></div>
                            </div>      
                        </form>
                    </div>
                </div>    
            </div>
            <div class="row footer">
                <div class="col-md-11 text-right">
                    <ul class="social-icons">
                        <li><a class="icon-facebook" href="https://www.facebook.com/Vertical3w"></a></li>
                        <li><a class="icon-twitter" href="https://twitter.com/vertical3w"></a></li>
                        <li><a class="icon-linkedin" href="http://www.linkedin.com/company/vertical3w"></a></li>
                    </ul>  
                </div>
            </div>
        </div>
        <script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/landingpage.min.js"></script>
    </body>
</html>